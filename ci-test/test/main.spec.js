const request = require("supertest");
const assert = require('assert');
const app = require("../main");

// GET
describe("GET /", function() {
	it("it should has status code 200", function(done) {
		request(app)
			.get("/")
			.expect(200)
			.end(function(err, res) {
				if (err) done(err);
				done();
			});
	});

	it("it shoud has response with hope key with value of loop", function(done) {
		request(app)
			.get("/")
			.expect({ hope: "loop" })
			.end(function(err, res) {
				if (err) done(err);
				done();
			});
	});
});

// POST
describe("POST /", function () {
	it("it should return status code 200 is name exists", function (done) {
		request(app)
			.post("/")
			.send({ name: "Hope"})
			.expect(200)
			.end(function (err, res) {
				if(err) done(err);
				done();
			});
	});
	it("it should return status code 400 if we doesn't send anything", function(done) {
		request(app)
			.post("/")
			.send({})
			.expect(400)
			.end(function (err, res) {
				if(err) done(err);
				done();
			});
	});
});
