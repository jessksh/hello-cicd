const supertest = require("supertest");
const request = supertest('http://localhost:8080');
const assert = require('assert');

// GET
describe("GET /hello/jess/Kim TEST", function() {
    it("[200] Message was correct. ", function(done) {
        request
            .get("/hello/jess/Kim")
            .expect("{\"message\":\"Hello jess Kim\"}")
            .expect(200, done)
    });

    it("GET Hello Test 2 ( Should occur an Error )", function(done) {
        request
            .get("/hello/sanghun/Kim")
            .expect('Content-Type', 'text/plain;charset=UTF-8')
            .expect("{\"message\":\"Hello sanghun Kim\"}")
            .expect(200, done)
    });
});
